import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
// Create a CrimeReport class
class CrimeReport {
    private String name;
    private String address;
    private String crimeType;
    private int age;
    private String gender;
    // Constructor to initialize object
    public CrimeReport(String name, String address, String crimeType, int age, String gender) {
        this.name = name;
        this.address = address;
        this.crimeType = crimeType;
        this.age = age;
        this.gender = gender;
    }
    // Method to print Crime Report
    public void printReport() {
        JOptionPane.showMessageDialog(null, "Name: " + name + "\nAddress: " + address + "\nCrime Type: " + crimeType + "\nAge: " + age + "\nGender: " + gender);
    }
}
// Create a CrimeReportSystem class with GUI
class CrimeReportSystemGUI extends JFrame implements ActionListener {
    private JLabel nameLabel, addressLabel, crimeTypeLabel, ageLabel, genderLabel;
    private JTextField nameField, addressField, crimeTypeField, ageField, genderField;
    private JButton submitButton;
    public CrimeReportSystemGUI() {
        // Set window title
        setTitle("Crime Report System");
        // Set window size
        setSize(400, 300);
        // Set layout manager
        setLayout(new GridLayout(6, 2));
        // Create labels and text fields
        nameLabel = new JLabel("Name:");
        nameField = new JTextField();
        addressLabel = new JLabel("Address:");
        addressField = new JTextField();
        crimeTypeLabel = new JLabel("Crime Type:");
        crimeTypeField = new JTextField();
        ageLabel = new JLabel("Age:");
        ageField = new JTextField();
        genderLabel = new JLabel("Gender:");
        genderField = new JTextField();
        // Create submit button
        submitButton = new JButton("Submit");
        // Add labels and text fields to window
        add(nameLabel);
        add(nameField);
        add(addressLabel);
        add(addressField);
        add(crimeTypeLabel);
        add(crimeTypeField);
        add(ageLabel);
        add(ageField);
        add(genderLabel);
        add(genderField);
        // Add submit button to window
        add(submitButton);
        // Register button listener
        submitButton.addActionListener(this);
        // Show window
        setVisible(true);
    }
    // ActionListener method to handle button click
    public void actionPerformed(ActionEvent e) {
        // Get user input
        String name = nameField.getText();
        String address = addressField.getText();
        String crimeType = crimeTypeField.getText();
        int age = Integer.parseInt(ageField.getText());
        String gender = genderField.getText();
        // Create CrimeReport object
        CrimeReport report = new CrimeReport(name, address, crimeType, age, gender);
        // Print Crime Report
        report.printReport();
    }
}
// Main class to launch the GUI
class Main {
    public static void main(String[] args) {
        new CrimeReportSystemGUI();
    }
}