#include <iostream>
using namespace std;
class Animal {
public:
    void eat() {
        cout << "Eating\n";
    }
};
class Dog : public Animal {
public:
    void bark() {
        cout << "Barking\n";
    }
};
class Cat : public Animal {
public:
    void meow() {
        cout << "Meowing\n";
    }
};
class CatDog : public Dog, public Cat {
};
int main() {
    CatDog cd;
    cd.bark();  
    cd.meow();  
    cd.eat();   
    return 0;
}