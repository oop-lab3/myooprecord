public class MutImt {
    private String s;
    MutImt(String s) {
        this.s = s;
    }
    public String getName() {
        return s;
    }
    public void setName(String coursename) {
        this.s = coursename;
    }
    public static void main(String[] args) {
        MutImt obj = new MutImt("MUTABLE");
        System.out.println(obj.getName());
        obj.setName("IMMUTABLE");
        System.out.println(obj.getName());
    }
}