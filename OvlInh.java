public class OvlInh {
    void m1(int a,int b){
        System.out.println("Sum is: "+(a+b));
    }
}
class Ovl extends OvlInh{
    void m1(int a,int b,int c){
        System.out.println("Sum is: "+(a+b+c));
    }
    public static void main(String args []) {
        Ovl obj = new Ovl();
        obj.m1(140 , 3);
        obj.m1(1 , 4 , 3);
    }
}