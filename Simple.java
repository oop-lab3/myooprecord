class Biryani {
    public void taste() {
        System.out.println("GOOD TASTE");
    }
}

class CB extends Biryani {
    void flavour() {
        System.out.println("GOOD TASTE WITH FLAVOUR");
    }
}

public class Simple {
    public static void main(String args[]) {
        CB b1 = new CB();
        b1.taste();
        b1.flavour();
    }
}