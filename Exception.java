public class Exception {
    public static void main(String args[]){
        try {
            int a = 3, b = 0;
            int c = a / b; 
            System.out.println("Result = " + c);
        }
        catch (ArithmeticException obj) {
            System.out.println("Can't divide an integer by 0");
        }
        try {
            int a[] = new int[5];
            a[10] = 20;
        }
        catch (ArrayIndexOutOfBoundsException obj) {
            System.out.println("Array Index is Out Of Bounds");
        }
        String s = new String("Welcome");
        Object o = (Object)s;
        Object o1 = new Object();
        String s1 = (String)o1;
    }
}