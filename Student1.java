public class Student{
    String fullName;
       double semPercentage;
       String collegeName;
       int collegeCode;
     
   Student(){   //Default constructor
       String collegeName="MVGR";
       int collegeCode=33;
       System.out.println("College Name : "+collegeName);
      System.out.println("College Code: "+collegeCode);
   }
   Student(String name,double sem){     //Parameterized Constructor
       fullName=name;
       semPercentage=sem;
       System.out.println("Name of student : "+fullName);
      System.out.println("Sempercentage of student : "+semPercentage);
   }
   public void finalize(){
        System.out.println("TERMINATE");
    }

   public static void main(String[] args){
                  new Student("SRI",97.7);
             new Student();
     System.gc();
   }
}