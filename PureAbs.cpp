#include<iostream>
using namespace std;
class Base{
    public:
    virtual void display() = 0;
};
class Derived:public Base{
    public:
    void display(){
        cout << "THE DERIVED CLASS";
    }
};
int main(){
    Derived obj ;
    obj.display();
}