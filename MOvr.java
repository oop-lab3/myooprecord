public class MOvr {
    public int m1(int a,int b){
        return a*b;
    }
    public float m1(float a,float b){
        return a*b;
    }
    public static void main(String args[]){
        MOvr obj = new MOvr();
        obj.m1(2, 3);
        obj.m1(5.0, 6.2);
    }
}